<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nilai</title>
</head>

<body>
    <h1>Berlatih Nilai PHP</h1>
    <?php
    function tentukan_nilai($number)
    {
        switch($number){
            case ($number >= 85 && $number <= 100):
                return "Sangat Baik<br>";
            case ($number >= 70 && $number < 85):
                return "Baik<br>";
            case ($number >= 60 && $number < 70):
                return "Cukup<br>";
            default:
                return "Kurang<br>";
        }
    }

    //TEST CASES
    echo tentukan_nilai(98); //Sangat Baik
    echo tentukan_nilai(76); //Baik
    echo tentukan_nilai(67); //Cukup
    echo tentukan_nilai(43); //Kurang
    ?>
</body>

</html>
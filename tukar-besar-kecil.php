<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tukar-besar-kecil</title>
</head>

<body>
    <h2>Tukar Besar Kecil</h2>
    <?php
    function tukar_besar_kecil($string)
    {  
        $converted = '';
        for($i = 0; $i < strlen($string); $i++){
            $char = ord($string[$i]);
            if($char >= 65 && $char <= 90){
                $char += 32;
                $converted .= chr($char);
            } else if($char >= 97 && $char <= 122){
                $char -= 32;
                $converted .= chr($char);
            } else{
                $converted .= $string[$i];
            }
        }
        return $converted;
    }
    // TEST CASES
    echo tukar_besar_kecil('Hello World') . "<br>"; // "hELLO wORLD"
    echo tukar_besar_kecil('I aM aLAY') . "<br>"; // "i Am Alay"
    echo tukar_besar_kecil('My Name is Bond!!') . "<br>"; // "mY nAME IS bOND!!"
    echo tukar_besar_kecil('IT sHOULD bE me') . "<br>"; // "it Should Be ME"
    echo tukar_besar_kecil('001-A-3-5TrdYW') . "<br>"; // "001-a-3-5tRDyw"

    ?>
</body>

</html>
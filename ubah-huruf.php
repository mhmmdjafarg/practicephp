<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Huruf</title>
</head>

<body>
    <h2>Ubah Huruf</h2>
    <?php
    function ubah_huruf($string)
    {
        $newString = '';
        for($i =0; $i < strlen($string); $i++){
            $nextchar = ord($string[$i])+1;

            // handle untuk kasus huruf 'z'
            if($nextchar === 123){
                $newString .= 'a';
            } else{
                $newString .= chr($nextchar);
            }
            
        }
        return $newString . "<br>";
    }

    // TEST CASES
    
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu

    ?>
</body>

</html>